import { ConnectionOptions } from 'typeorm'

const config: ConnectionOptions = {
    type: 'mssql',
    host: 'localhost',
    synchronize: true,
    logging: false,
    entities: [
        'src/entity/**/*.ts'
    ],
    migrations: [
        'src/migration/**/*.ts'
    ],
    subscribers: [
        'src/subscriber/**/*.ts'
    ],
    cli: {
        entitiesDir: 'src/entity',
        migrationsDir: 'src/migration',
        subscribersDir: 'src/subscriber'
    }
}

export = config
