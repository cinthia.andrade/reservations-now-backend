import {
    Entity,
    PrimaryColumn,
    Column,
    UpdateDateColumn,
    CreateDateColumn,
} from "typeorm";


@Entity ("admin")
class Admin {
    @PrimaryColumn({ type: "int" })
    readonly id: number;

    @Column ({ type: "int"})
    id_restaurante: number;

    @Column ({ type: "varchar"})
    nome: string;

    @Column ({ type: "varchar"})
    senha: string;

    @Column ({ type: "varchar"})
    email: string;

}

export default Admin;