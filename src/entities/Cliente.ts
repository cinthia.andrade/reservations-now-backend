import {
    Entity,
    PrimaryColumn,
    Column,
    UpdateDateColumn,
    CreateDateColumn,
} from "typeorm";


@Entity ("cliente")
class Cliente {
    @PrimaryColumn({ type: "int" })
    readonly id: number;

    @Column ({ type: "varchar"})
    nome: string;

    @Column ({ type: "varchar"})
    email: string;

    @Column ({ type: "varchar"})
    senha: string;


}

export default Cliente;