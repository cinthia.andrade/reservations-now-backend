import {
    Entity,
    PrimaryColumn,
    Column,
    UpdateDateColumn,
    CreateDateColumn,
} from "typeorm";


@Entity ("comanda")
class Comanda {
    @PrimaryColumn({ type: "int" })
    readonly id: number;

    @Column({ type: "int"})
    readonly id_restaurante: number;

    @Column ({ type: "int"})
    readonly id_mesa: number;
    
    @Column ({ type: "int"})
    readonly id_garcom: number;

    @Column ({ type: "int"})
    readonly id_cliente: number;

    @Column ({ type: "int"})
    ordem: number;

    @Column ({ type: "varchar"})
    data_aberta: string;

    @Column ({ type: "varchar"})
    data_fechada: string;

    @Column ({ type: "varchar"})
    status_comanda: string;

    @Column ({ type: "decimal"})
    total_conta: number;

}

export default Comanda;