import {
    Entity,
    PrimaryColumn,
    Column,
    UpdateDateColumn,
    CreateDateColumn,
} from "typeorm";


@Entity ("garcom")
class Garcom {
    @PrimaryColumn({ type: "int" })
    readonly id: number;

    @Column ({ type: "varchar"})
    nome: string;

    @Column ({ type: "int"})
    id_restaurante: number;


}

export default Garcom;