import {
    Entity,
    PrimaryColumn,
    Column,
    UpdateDateColumn,
    CreateDateColumn,
} from "typeorm";


@Entity ("mesa")
class Mesa {
    @PrimaryColumn({ type: "int" })
    readonly id: number;
}

export default Mesa;