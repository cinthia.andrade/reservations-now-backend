import {
    Entity,
    PrimaryColumn,
    Column,
    UpdateDateColumn,
    CreateDateColumn,
} from "typeorm";


@Entity ("mesaRestaurante")
class MesaRestaurante {
    @PrimaryColumn({ type: "int" })
    readonly id_mesa: number;

    @Column({ type: "int" })
    readonly id_restaurante: number;

    @Column({ type: "smallint" })
    capacidade: number;

    @Column ({ type: "varchar"})
    status: string;

}

export default MesaRestaurante;