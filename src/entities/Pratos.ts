import {
    Entity,
    PrimaryColumn,
    Column,
    UpdateDateColumn,
    CreateDateColumn,
} from "typeorm";


@Entity ("pratos")
class Pratos {
    @PrimaryColumn({ type: "int" })
    readonly id: number;

    @Column({ type: "int" })
    readonly id_restaurante: number;

    @Column({ type: "varchar" })
    nome: string;

    @Column({ type: "decimal" })
    preco: number;

    @Column({ type: "varchar" })
    descricao: string;

    @Column({ type: "varchar" })
    categoria: string;

    @Column({ type: "varbinary" })
    foto: number;
}

export default Pratos;