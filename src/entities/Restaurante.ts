import {
    Entity,
    PrimaryColumn,
    Column,
    UpdateDateColumn,
    CreateDateColumn,
} from "typeorm";


@Entity ("restaurante")
class Restaurante {
    @PrimaryColumn({ type: "int" })
    readonly id: number;

    @Column({ type: "varchar" })
    nome: string;

    @Column({ type: "varchar" })
    descricao: string;

    @Column({ type: "varchar" })
    endereco: string;

    @Column({ type: "varchar" })
    telefone: string;


}

export default Restaurante;