import cors from 'cors';
import express from 'express';
import routes from './routes/main'
import { getConnectionManager } from 'typeorm';
require('dotenv').config();

const { DB_USERNAME, DB_PASSWORD } = process.env;

const cm = getConnectionManager()
const con = cm.create({
    type: 'mssql',
    host: 'localhost',
    database: 'ReservationsNow',
    username: DB_USERNAME,
    password: DB_PASSWORD,
    options: {
        encrypt: false
    }
})

con.connect().then(
    () => console.log('conectou!')
).catch(
    err => console.log(err)
)
const app = express()

app.use(express.json())
app.use(cors())


routes(app)


app.listen(5000, 'localhost', () => {
    console.log('API listening on 5000')
})
