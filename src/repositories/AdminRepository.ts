import {Repository, EntityRepository} from "typeorm";
import Admin from "../entities/Admin";


@EntityRepository(Admin)
class AdminRepository extends Repository<Admin> {}

export default AdminRepository;