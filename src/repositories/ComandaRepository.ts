import {Repository, EntityRepository} from "typeorm";
import Comanda from "../entities/Comanda";


@EntityRepository(Comanda)
class ComandaRepository extends Repository<Comanda> {}

export default ComandaRepository;