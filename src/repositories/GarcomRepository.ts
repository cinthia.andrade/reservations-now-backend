import {Repository, EntityRepository} from "typeorm";
import Garcom from "../entities/Garcom";


@EntityRepository(Garcom)
class GarcomRepository extends Repository<Garcom> {}

export default GarcomRepository;