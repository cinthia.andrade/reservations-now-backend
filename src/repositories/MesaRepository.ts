import {Repository, EntityRepository} from "typeorm";
import Mesa from "../entities/Mesa";


@EntityRepository(Mesa)
class MesaRepository extends Repository<Mesa> {}

export default MesaRepository;