import {Repository, EntityRepository} from "typeorm";
import MesaRestaurante from "../entities/MesaRestaurante";

@EntityRepository(MesaRestaurante)
class MesaRestauranteRepository extends Repository<MesaRestaurante> {}

export default MesaRestauranteRepository;