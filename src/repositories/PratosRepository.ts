import {Repository, EntityRepository} from "typeorm";
import Pratos from "../entities/Pratos";

@EntityRepository(Pratos)
class PratosRepository extends Repository<Pratos> {}

export default PratosRepository;