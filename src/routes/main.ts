import express from 'express'
import testeRoutes from './teste'


export default function registerRoutes (app: express.Express) {
    testeRoutes(app)
}
