import express from 'express'


export default function testeRoutes (app: express.Express) {
    app.post('/teste', (req, res) => {
        console.log(req.body)
        res.json({
            message: 'Ok'
        })
    })

    app.get('/', (req, res) => {
        res.json({
            message: 'Hello World!'
        })
    })
}
